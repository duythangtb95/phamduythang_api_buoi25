// link mockapi
const Base_URL = "https://636a03a6b10125b78fce1342.mockapi.io";

// function render
function feltchaALLTodos() {
    turnOnLoading();
    axios({
        url: `${Base_URL}/todos`,
        method: "GET",
    })
        .then(function (res) {
            turnOffLoading();
            console.log("res: ", res)
            renderTodos(res.data);
        })
        .catch(function (err) {
            turnOffLoading();
            console.log("err: ", err);
        });
}

// function render data
function renderTodos(todos) {
    var contentHTML = "";
    todos.forEach(function (item) {
        var contentTr =
            `<tr>
                <td>${item.id}</td>
                <td>${item.name}</td>
                <td>${item.decs}</td>
                <td>
                <input type="checkbox" ${item.isComplete? "checked" : ""}>
                </td>
                <td>
                <button class="btn btn-danger" onclick="removeTodos(${item.id})">Delete</button>
                </td>
                <td>
                <button class="btn btn-primary" onclick="editTodos(${item.id})">Edit</button>
                </td>
            </tr>`;
        contentHTML += contentTr;
    });
    document.getElementById('tbody-todos').innerHTML = contentHTML;
}

// bật tắt Loading
function turnOnLoading(){
    document.getElementById("loading").style.display = "flex";
}

function turnOffLoading(){
    document.getElementById("loading").style.display = "none";
}

// lấy thông tin từ form
function getDatatForm(){
    var newName = document.getElementById('postNewName').value;
    var newDecs = document.getElementById('postNewDecs').value;
    return{
        name: newName,
        decs: newDecs,
    }
}