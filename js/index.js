// render all todos 
feltchaALLTodos();

// remove todo service
function removeTodos(idTodos) {
    turnOnLoading();
    axios({
        url: `${Base_URL}/todos/${idTodos}`,
        method: "DELETE",
    })
        .then(function (res) {
            turnOffLoading();
            feltchaALLTodos();
            console.log("res: ", res.data);
        })
        .catch(function (err) {
            turnOffLoading();
            console.log("err: ", err);
        });

}

// post new todos
function postNewTodos() {
    turnOnLoading();
    var data = getDatatForm();
    var NewTodos = {
        name: data.name,
        decs: data.decs,
        isComplete: true,
    };
    axios({
        url: `${Base_URL}/todos`,
        method: "POST",
        data: NewTodos,
    })
        .then(function (res) {
            turnOffLoading();
            feltchaALLTodos();
            console.log("res: ", res.data);
        })
        .catch(function (err) {
            turnOffLoading();
            console.log("err: ", err);
        });
}

// edit todos
var idEdit = null;

function editTodos(idTodos) {
    turnOnLoading();
    axios({
        url: `${Base_URL}/todos/${idTodos}`,
        method: "GET",
    })
        .then(function (res) {
            document.getElementById('postNewTodos').classList = "d-none";
            document.getElementById('updateTodos').classList = "btn btn-primary mx-auto my-4 d-block";
            document.getElementById("postNewName").value = res.data.name;
            document.getElementById("postNewDecs").value = res.data.decs;
            idEdit = res.data.id;
            turnOffLoading();
            feltchaALLTodos();
            console.log("res: ", res.data);
        })
        .catch(function (err) {
            turnOffLoading();
            console.log("err: ", err);
        });
}

// update todos
function updateTodos(){
    turnOnLoading();
    let data = getDatatForm();
    axios({
        url: `${Base_URL}/todos/${idEdit}`,
        method: "PUT",
        data: data,
    })
        .then(function (res) {
            turnOffLoading;
            console.log(res);
            feltchaALLTodos();
            document.getElementById('postNewTodos').classList = "btn btn-warning mx-auto d-block";
            document.getElementById('updateTodos').classList = "d-none";
        })
        .catch(function (err) {
            turnOffLoading();
            console.log(err);
        });
}